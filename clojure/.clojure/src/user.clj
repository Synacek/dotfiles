;(in-ns 'user)
;(require '[dev.nu.morse :as morse :refer [inspect launch-in-proc]])
;(println "LOADED personal user.clj")

(ns user 
  (:require
   [clojure.java.io :as io]))

(comment ;;from https://clojure-goes-fast.com/blog/system-wide-user-clj/ but should go to project user.clj
 (when *file*
   (->> (.getResources (.getContextClassLoader (Thread/currentThread)) "user.clj")
        enumeration-seq
        ;; Assume the first user.clj is the currently loaded one. Load others if
        ;; there are more to load.
        rest
        (run! #(clojure.lang.Compiler/load (clojure.java.io/reader %))))))


(comment  ;;from https://clojureverse.org/t/how-are-user-clj-files-loaded/3842/3
  (defn- static-classpath-dirs
    []
    (mapv #(.getCanonicalPath  %) (classpath/classpath-directories)))

  (defn user-clj-paths
    []
    (->> (static-classpath-dirs)
      (map #(io/file % "user.clj"))
      (filter #(.exists %))))

  (defn load-user!
    [f]
    (try
      (prn (str "Loading " f))
      (load-file (str f))
      (catch Exception e
        (binding [*out* *err*]
          (printf "WARNING: Exception while loading %s\n" f)
          (prn e)))))

  (defn load-all-user!
    []
    (let [paths (user-clj-paths)]
      (prn (str "Load " (first paths)))
      (doall
        (map load-user! (rest paths)))))

  (load-all-user!))



(println "Loaded system-wide user.clj!")

