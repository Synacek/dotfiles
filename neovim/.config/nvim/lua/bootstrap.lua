local PKGS = {
  "savq/paq-nvim";
  "folke/tokyonight.nvim";
  "gpanders/nvim-parinfer";
  "jpalardy/vim-slime";
  "neovim/nvim-lspconfig";
  "nvim-lua/plenary.nvim";
  "nvim-telescope/telescope.nvim";
  "nvim-telescope/telescope-ui-select.nvim";
  "sindrets/diffview.nvim";
  "NeogitOrg/neogit";
  "MunifTanjim/nui.nvim";
  "jackMort/ChatGPT.nvim";
  "pasky/claude.vim";
  "Robitx/gp.nvim";
  -- List your packages here!
}
local function clone_paq()
  local path = vim.fn.stdpath('data') .. '/site/pack/paqs/start/paq-nvim'
  if vim.fn.empty(vim.fn.glob(path)) > 0 then
    vim.fn.system {
      'git',
      'clone',
      '--depth=1',
      'https://github.com/savq/paq-nvim.git',
      path
    }
  end
end
local function bootstrap_paq()
  clone_paq()
  -- Load Paq
  vim.cmd('packadd paq-nvim')
  local paq = require('paq')
  -- Exit nvim after installing plugins
  vim.cmd('autocmd User PaqDoneInstall quit')
  -- Read and install packages
  paq(PKGS)
  paq.install()
end
return { bootstrap_paq = bootstrap_paq }
