vim.api.nvim_create_autocmd(
    {"BufNewFile", "BufRead"},
    {
        pattern = "*.bb",
        callback = function()
           local buf = vim.api.nvim_get_current_buf()
           vim.api.nvim_buf_set_option(buf, "filetype", "clojure")
        end
    }
)



require("tokyonight").setup({
  -- your configuration comes here
  -- or leave it empty to use the default settings
  style = "night"
});
vim.cmd[[colorscheme tokyonight]];



local oldstyleinit = vim.fn.stdpath("config") .. "/init-old-style.vim"
vim.cmd.source(oldstyleinit)

-- inpiration for lsp config: https://vonheikemen.github.io/devlog/tools/neovim-lsp-client-guide/

local lspconfig = require('lspconfig')

-- reserve space in left column for lsp warnings
vim.opt.signcolumn = 'yes'

-- borderes for lsp dialogs
vim.diagnostic.config({
  float = {
    border = 'rounded',
  },
})
vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
  vim.lsp.handlers.hover,
  {border = 'rounded'}
)

-- diagnostic only in normal mode
vim.api.nvim_create_autocmd('ModeChanged', {
  pattern = {'n:i', 'v:s'},
  desc = 'Disable diagnostics in insert and select mode',
  callback = function(e) vim.diagnostic.disable(e.buf) end
})
vim.api.nvim_create_autocmd('ModeChanged', {
  pattern = 'i:n',
  desc = 'Enable diagnostics when leaving insert mode',
  callback = function(e) vim.diagnostic.enable(e.buf) end
})

-- corect the colors
local function hide_semantic_highlights()
  for _, group in ipairs(vim.fn.getcompletion('@lsp', 'highlight')) do
    vim.api.nvim_set_hl(0, group, {})
  end
end
vim.api.nvim_create_autocmd('ColorScheme', {
  desc = 'Clear LSP highlight groups',
  callback = hide_semantic_highlights,
})


vim.keymap.set('n', '<C-b>', '<cmd>:Telescope buffers<cr>')

-- lsp keybindings
vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function(event)
    local bufmap = function(mode, lhs, rhs)
      local opts = {buffer = event.buf}
      vim.keymap.set(mode, lhs, rhs, opts)
    end

    -- Trigger code completion
    bufmap('i', '<C-Space>', '<C-x><C-o>')

    -- Display documentation of the symbol under the cursor
    bufmap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>')

    -- Jump to the definition
    bufmap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>')

    -- Jump to declaration
    -- bufmap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>')

    -- Lists all the implementations for the symbol under the cursor
    -- bufmap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>')

    -- Jumps to the definition of the type symbol
    -- bufmap('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>')

    -- Lists all the references 
    -- bufmap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>')
    bufmap('n', 'gr', '<cmd>:Telescope lsp_references<cr>')

    -- Displays a function's signature information
    bufmap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<cr>')

    -- Renames all references to the symbol under the cursor
    bufmap('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>')

    -- Format current file
    bufmap('n', '<F3>', '<cmd>lua vim.lsp.buf.format()<cr>')

    -- Selects a code action available at the current cursor position
    bufmap('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>')

    bufmap('n', '<F5>', '<cmd>lua vim.lsp.buf.completion()<cr>')

    bufmap('n', 'gs', '<cmd>:Telescope lsp_workspace_symbols<cr>')

  end
})

-- telescope keybindings
vim.keymap.set('n', '<C-p>', '<cmd>Telescope find_files<cr>')
vim.keymap.set('n', '<C-f>', '<cmd>Telescope live_grep<cr>')
require("telescope").load_extension("ui-select")

-- slime sent to REPL the outer most form
vim.keymap.set('n', '<C-x>', "99[(va(<Plug>SlimeRegionSend")

-- local util = require 'lspconfig.util'
-- lspconfig.clojure_lsp.setup {root_dir = util.root_pattern(".git")}
lspconfig.clojure_lsp.setup {root_dir = vim.loop.cwd}
lspconfig.terraformls.setup {}

-- GIT
local neogit = require('neogit')
neogit.setup {
  graph_style = "unicode",
  commit_editor = {
    kind = "split",
  },
  rebase_editor = {
    kind = "split",
  },
  merge_editor = {
    kind = "split",
  }
}

-- ChatGPT
require("chatgpt").setup({
      -- this config assumes you have OPENAI_API_KEY environment variable set
      openai_params = {
        model = "gpt-4-1106-preview",
        frequency_penalty = 0,
        presence_penalty = 0,
        max_tokens = 4095,
        temperature = 0.2,
        top_p = 0.1,
        n = 1,
      }
    });


-- Claude.vim
vim.g.claude_use_bedrock = true
vim.g.claude_bedrock_region = 'eu-central-1'
vim.g.claude_aws_profile = 'claude.vim'

-- gp.nvim

require("gp").setup({

  openai_api_key = os.getenv("OPENAI_API_KEY"),
  chat_user_prefix = "VSY:",
  chat_assistant_prefix = { "AI", "({{agent}}):" },

  providers = {
    pplx = { 
 			disable = false,
    },
    anthropic = { 
 			disable = false,
    },
  },

  agents = {
     { 
 			provider = "pplx", 
 			name = "PPLXSmall", 
 			chat = true, 
 			command = false, 
 			-- string with model name or table with model name and parameters 
 			model = { model = "llama-3.1-sonar-small-128k-chat", temperature = 1.1, top_p = 1 }, 
 			-- system prompt (use this to specify the persona/role of the AI) 
 			system_prompt = require("gp.defaults").chat_system_prompt, 
 		 }, 
     { 
 			provider = "pplx", 
 			name = "PPLXLarge", 
 			chat = true, 
 			command = false, 
 			-- string with model name or table with model name and parameters 
 			model = { model = "llama-3.1-sonar-large-128k-chat"}, 
 			-- system prompt (use this to specify the persona/role of the AI) 
 			system_prompt = require("gp.defaults").chat_system_prompt, 
     },
     { 
 			provider = "anthropic", 
 			name = "Claude", 
 			chat = true, 
 			command = false, 
 			-- string with model name or table with model name and parameters 
 			model = { model = "claude-3-5-sonnet-20241022", temperature = 0.8, top_p = 1 }, 
 			-- system prompt (use this to specify the persona/role of the AI) 
 			system_prompt = require("gp.defaults").chat_system_prompt, 
 		 },
     { 
 			provider = "openai", 
 			name = "ChatGPT4o-mini",
      disable = true,
     },
     { 
 			provider = "pplx", 
 			name = "ChatPerplexityLlama3.1-8B",
      disable = true,
     },
     { 
 			provider = "anthropic", 
 			name = "ChatClaude-3-5-Sonnet",
      disable = true,
     },
     { 
 			provider = "anthropic", 
 			name = "ChatClaude-3-Haiku",
      disable = true,
     },
  },

});

vim.keymap.set('n', '<C-g>t', '<cmd>:GpChatToggle<cr>')
