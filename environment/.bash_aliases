#alias nvim=/home/vasek/Downloads/nvim-linux64/bin/nvim
alias vim=nvim
alias vi=nvim

# editing with history
# Up arrow
bind '"\e[A": history-search-backward'
# Down arrow
bind '"\e[B": history-search-forward'

# expand !! !$ and !* by following them by space
bind Space:magic-space
