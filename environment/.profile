PATH="$HOME/bin:$PATH"
source ~/.variables

if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ] && [ "$HOSTNAME" = dubobeh ]; then
  startx
fi
